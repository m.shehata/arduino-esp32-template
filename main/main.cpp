#include "driver/gpio.h"
#include "esp_system.h"
#include <Arduino.h>

#define GPIO_OUTPUT_PIN_SEL  (1ULL<<GPIO_NUM_2) // Pin D2 corresponds to GPIO2

extern "C" void app_main(void) {
    printf("BASM ALLAH\n");

    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_INTR_DISABLE; // Disable interrupt
    io_conf.mode = GPIO_MODE_OUTPUT; // Set as output mode
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL; // Bit mask for the pin
    gpio_config(&io_conf);

    int x = 0;

    while(true) {
        delay(1000);
        printf("Counter => %d\n", x++);

        gpio_set_level(GPIO_NUM_2, x % 2);
    }
}